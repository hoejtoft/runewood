@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <br />
            <h3>Add or Edit gallery</h3>
            <br />
            @if(count($errors) > 0)

                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                    @endif
                    <form method="post" enctype="multipart/form-data" action="{{action('HomeController@update', 1)}}">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="PATCH" />
                        <div class="form-group">
                            <div class="row no-margin">
                            @foreach( $gallery as $row)

                                        <div class="col-md-6">
                                            <label class="control-label" for="imageupload"><i class="fa fa-envelope"></i></label>
                                            <input type="file" name="{{"imagepath".$row->id}}" id="{{"imagepath".$row->id}}" class="filestyle">
                                            <img width="100%" id="{{"tempimg".$row->id}}" src="" style="display:none;">
                                            <div class="editimg" id="{{"editimg_".$row->id}}">

                                                <img src="{{ asset('/img/uploads/news/image/'.$row->imagepath) }}">
                                            </div>
                                            <a class="btn btn-danger admincontrol delete-button"  id="{{$row->id}}" href="{{route('home.deleteslide',['id' => $row->id ])}}"><i class="fa fa-times"></i>Delete</a>
                                        </div>


                            @endforeach
                            </div>
                        </div>

                                                <div class="form-group">
                            <div class="text-center">
                                <a class="btn btn-warning" href="javascript:history.back()"><i class="fa fa-chevron-left" style="margin-right:5px;"></i>Cancel</a>
                                <button type="submit" class="btn btn-primary calltoaction"><i class="fa fa-newspaper" style="margin-right:5px;"></i>Update gallery</button>

                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </div>

@endsection