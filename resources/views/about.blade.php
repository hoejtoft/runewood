@extends('master')

@section('about')
    <div class="container">
    <div class="row centered">
        <div class="col-md-12">
            <h1 class="text-center Headline" style="font-size: 40px;">About</h1>

            <div class="single-text-section ContentMaxwidth">
                <hr>


                   <p class="breadtext" style="white-space:pre-line"> Located in Denmark, the heart of Jutland - Herning, Runewood was born and came to be, by the idea of creating an environment to bring people closer together. Through the ages people have gathered to share a good quality drink, and today is no different. It is through these moments that great stories are told, emotions are shared and people come closer together. This is more important now than ever before, as technology tend to drive us further apart. However, at Runewood we aim to use technology to bring people closer, by integrating unique design features.</p>
                    <img width="100%" src="{{ asset('/img/candle.jpg') }}">
                    <p class="breadtext" style="white-space:pre-line">
                    New local breweries are growing in popularity, and the search for fine spirits has only increased. At Runewood we aim to offer these treasured drinks a decent home, but also to invoke an experience worth sharing, such that new stories can be born and relationships maintained. The essence here is to value the experience and the social time just as much as the drink itself.

                    The Nordic feel, and touch is the heart of our designs, which is brought to life with passionate engineering and an eye for detail. It is our goal to be innovative, creative and to think new, though at the same time hold close the values of old. Natural materials combined with modern technology remind us of the past, whilst propelling us towards the future - keeping alive the Danish design.

                    At the end of the day everyone should be able to enjoy a good glass, from their own personal bar. May it be cherished in silence or with good friends, we at Runewood will do our best, that whenever the moment comes it will be an experience.
                    </p>

            </div>
        </div>
    </div>
    </div>
    <div class="fullwith_container">
        <div class="container">
        <div class="row">
            <div class="ContentMaxwidth">
                <div class="col-md-6 float-left">
                    <div class="text-center">
                        <p class="runeword">Mission</p>
                    </div>
                    <p class="About-title text-center">Our mission</p>

                    <hr>
                    <p class="About-content" style="margin-bottom: 50px;">Our designs will be the state of the art and of highest quality, enhancing a great experience for you to enjoy a glass of rum, gin or whisky - whatever your heart desire. The nordic feel will be an important element in our designs to share the Danish way of living.</p>
                </div>
                <div class="col-md-6 float-right">
                    <div class="text-center">
                        <p class="runeword">Vision</p>
                    </div>
                    <p class="About-title text-center">Our vision</p>

                    <hr>
                    <p class="About-content">In a busy society as today, it is our hope to offer a cool way to relax and enjoy the moment. Having our designs to be more than simple furniture, we aim to create unique experiences and ultimately spreading the Danish <i>hygge</i>. Good stories are priceless, and are best shared with others. Therefore, at Runewood we are ambitious to make designs not only worthy of a story, but also to be the centre of where they are told. </p>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection