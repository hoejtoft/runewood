@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <br />
            <h3>Edit Record</h3>
            <br />
            @if(count($errors) > 0)

                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                    @endif
                    <form method="post" enctype="multipart/form-data" action="{{action('testimonialController@update', $id)}}">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="PATCH" />
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="testimonial_name"><i class="fa fa-font"></i></label>
                                <input class="form-control floatplacehold" value="{{$testimonial->testimonial_name}}" name="testimonial_name" id="testimonial_name" placeholder="Enter a name for testimonial author" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="testimonial_profession"><i class="fa fa-font"></i></label>
                                <input class="form-control floatplacehold" value="{{ $testimonial->testimonial_profession }}" name="testimonial_profession" id="testimonial_profession" placeholder="Enter a profession for testimonial author (optional)" type="text">
                            </div>
                        </div>
                        {{--<div class="form-group">--}}
                            {{--<div class="col-sm-12">--}}
                                {{--<label class="control-label" for="testimonial_rating"><i class="fa fa-font"></i></label>--}}
                                {{--<input class="form-control" value="{{ $testimonial->testimonial_rating }}" name="testimonial_rating" id="testimonial_rating" placeholder="Enter 1-5 for rating" type="text">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="testimonial_text"><i class="fa fa-font"></i></label>
                                <input class="form-control floatplacehold" value="{{ $testimonial->testimonial_text }}" name="testimonial_text" id="testimonial_text" placeholder="Enter testimonial text" type="text">
                            </div>
                        </div>
                        {{--<div class="form-group">--}}
                            {{--<div class="col-sm-12">--}}
                                {{--<label class="control-label" for="testimonial_image"><i class="fa fa-font"></i></label>--}}
                                {{--<input class="form-control" value="{{ $testimonial->testimonial_image }}" name="testimonial_image" id="testimonial_image" placeholder="Enter url for image (Obtional)" type="text">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="col-sm-12">--}}
                                {{--<label class="control-label" for="testimonial_productcategory"><i class="fa fa-font"></i></label>--}}
                                {{--<select name="testimonial_productcategory" class="form-control selectpicker">--}}
                                    {{--<option>Select a category</option>--}}
                                    {{--<option {{$testimonial->testimonial_productcategory == "Cabinet" ? "selected" : ""}}>Cabinet</option>--}}
                                    {{--<option {{$testimonial->testimonial_productcategory == "Wallhanger" ? "selected" : ""}}>Wallhanger</option>--}}
                                {{--</select>--}}

                            {{--</div>--}}
                        {{--</div>--}}


                        <div class="form-group">
                            <div class="text-center">
                                <a class="btn btn-warning" href="javascript:history.back()"><i class="fa fa-chevron-left" style="margin-right:5px;"></i>Cancel</a>
                                <button type="submit" class="btn btn-primary calltoaction"><i class="fa fa-newspaper" style="margin-right:5px;"></i>Update testimonial</button>

                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </div>

@endsection