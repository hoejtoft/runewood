@extends('...master')

@section('content')


        <div class="row centered">
            <div class="col-md-12">
                <h1 class="text-center Headline" style="font-size: 40px;">Add new testimonial</h1>
                <hr>
                @if(count($errors) > 0)

                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                        @endif
                <form class="contactForm" method="post" enctype="multipart/form-data" action="{{url('testimonial')}}">
                    {{csrf_field()}}
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label" for="testimonial_name"><i class="fa fa-font"></i></label>
                            <input class="form-control floatplacehold" value="{{ old('testimonial_name') }}" name="testimonial_name" id="testimonial_name" placeholder="Enter a name for testimonial author" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label" for="testimonial_profession"><i class="fa fa-font"></i></label>
                            <input class="form-control floatplacehold" value="{{ old('testimonial_profession') }}" name="testimonial_profession" id="testimonial_profession" placeholder="Enter a profession for testimonial author (optional)" type="text">
                        </div>
                    </div>
                    {{--<div class="form-group">--}}
                        {{--<div class="col-sm-12">--}}
                            {{--<label class="control-label" for="testimonial_rating"><i class="fa fa-font"></i></label>--}}
                            {{--<input class="form-control" value="{{ old('testimonial_rating') }}" name="testimonial_rating" id="testimonial_rating" placeholder="Enter 1-5 for rating" type="text">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label" for="testimonial_text"><i class="fa fa-font"></i></label>
                            <input class="form-control floatplacehold" value="{{ old('testimonial_text') }}" name="testimonial_text" id="testimonial_text" placeholder="Enter testimonial text" type="text">
                        </div>
                    </div>
                    {{--<div class="form-group">--}}
                        {{--<div class="col-sm-12">--}}
                            {{--<label class="control-label" for="testimonial_image"><i class="fa fa-font"></i></label>--}}
                            {{--<input class="form-control" value="{{ old('testimonial_image') }}" name="testimonial_image" id="testimonial_image" placeholder="Enter url for image (Obtional)" type="text">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="form-group">--}}
                        {{--<div class="col-sm-12">--}}
                            {{--<label class="control-label" for="testimonial_productcategory"><i class="fa fa-font"></i></label>--}}
                            {{--<select name="testimonial_productcategory" class="form-control selectpicker">--}}
                                {{--<option>Select a category</option>--}}
                                {{--<option>Cabinet</option>--}}
                                {{--<option>Wallhanger</option>--}}
                            {{--</select>--}}

                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="form-group">
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary calltoaction"><i class="fa fa-newspaper" style="margin-right:5px;"></i>Add testimonial</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <div class="row">


    </div>

@endsection