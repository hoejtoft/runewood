@extends('../../master')

@section('content')
<div class="container vhcontainer">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1 class="text-center Headline" style="font-size: 40px;">Login</h1>


                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        @csrf



                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="email"><i class="fa fa-envelope"></i></label>
                                <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="email" value="{{ old('email') }}" placeholder="Enter your mail" type="email" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="password"><i class="fa fa-key"></i></label>
                                <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="password"  placeholder="Enter your password" type="password" required>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group">

                            <div class="col-md-7">
                                <button type="submit" class="btn btn-primary calltoaction">
                                    {{ __('Login') }}
                                </button>

                            </div>
                        </div>

                    </form>
                </div>

        </div>
    </div>
</div>
@endsection
