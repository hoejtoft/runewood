@extends('master')

@section('slider')
    <div class="row">
        <div class="col-md-12">

            <header>

                <div id="carouselExampleIndicators" class="carousel slide frontslider" data-ride="carousel">
                    <ol class="carousel-indicators">

                            <?php
                            $indicators = array();
                            ?>



                        @for($i = 0; $i<count($gallery); $i++)
                            @if($gallery[$i]->imagepath != "null")
                        <?php
                                        array_push($indicators,$i);
                                ?>
                            @endif
                            @endfor
                        @for($i = 0; $i<count($indicators); $i++)


                            <li data-target="#carouselExampleIndicators" id="{{"slideindicator_".$i}}" data-slide-to="{{intval(($i))}}"></li>

                        @endfor
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <!-- Slide One - Set the background image for this slide in the line below -->
                        @foreach($gallery as $row)
                            @if($row->imagepath != "null")
                            <div class="carousel-item" id="{{"slideitem_".$row->id}}">
                                <img src="{{ asset('/img/uploads/news/image/'.$row->imagepath) }}"  alt="cabinet" />
                                <div class="carousel-caption d-none d-md-block">

                                </div>
                            </div>
                            @endif
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </header>

        </div>
    </div>
    <div class="welcometext">
    <div class="container">
        <div class="welcome_section">
        <h1 class="text-center">Welcome to Runewood</h1>
        <p>
            Experience Danish design as never before. Passion for details and the aim to reach a high-end quality,
            we encourage you to explore our unique products. Our goal is to create something beautiful and worth sharing.
            <br><br>
            At Runewood we do our best, not only to build fine furniture, but also to create products that will be part of new and unforgettable experiences.
            All our designs are born in Denmark, and everything is hand build.
        </p>
        <h2 class="text-center"><i><span class="quotesign">"</span>Bringing alive the Danish hygge<span class="quotesign">"</span></i></h2>
        </div>
    </div>

    </div>
@endsection
@section('fullwidth')
{{--@if(auth()->guest())--}}
{{--@elseif(auth()->user()->userlevel == 1)--}}
    <div class="contentmaxwidth fullwidth-bg">

        <section class="reviews">
            <h4 class="main-title text-center font-weight-bold pb-4">WHAT CUSTOMERS SAY</h4>

            <div class="container">
                <div class="owl-carousel">

                    <!-- card -->
                    @foreach($testimonial as $row)

                        <div class="item">
                            <div class="card">
                                <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 p-col">
                                        <div class="text-center"><i class="fas fa-quote-right runewood_quote"></i></div>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 p-col">
                                        <p class="text-center">{{$row->testimonial_text}}</p>
                                </div>

                                    <div class="col-md-12 info-col">
                                        <h4 class="title text-center">{{$row->testimonial_name}}</h4>
                                        <h6 class="sub-title text-center">{{$row->testimonial_profession}}</h6>
                                        {{--<ul>--}}
                                            {{--@if($row->testimonial_rating == 0 || $row->testimonial_rating == null)--}}
                                            {{--@elseif($row->testimonial_rating == 1)--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--@elseif($row->testimonial_rating == 2)--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--@elseif($row->testimonial_rating == 3)--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--@elseif($row->testimonial_rating == 4)--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--@elseif($row->testimonial_rating == 5)--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--@endif--}}

                                        {{--</ul>--}}
                                    </div>

                                    {{--<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 img-col">--}}
                                        {{--<div class="user" style="background-image: url('{{$row->testimonial_image}}')"></div>--}}
                                    {{--</div>--}}


                                </div>
                            </div>
                @if(auth()->guest())
                @elseif(auth()->user()->userlevel == 1)
                <div class="crud-blok testimonial-crud">
                <a>
                <form  method="post" class="delete_form reset-this" action="{{action('testimonialController@destroy', $row->id)}}">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="DELETE" />

                                <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                            </form>
                </a>

                <a class="btn btn-warning admincontrol" id="delete_{{$row->id}}" href="{{action('testimonialController@edit',$row->id)}}"><i class="fa fa-edit"></i>Edit</a>
            </div>
                @endif
                        </div>
                    @endforeach







                </div>
            </div>
        </section>
    </div>
    {{--@endif--}}

@endsection