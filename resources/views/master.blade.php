<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Runewood - The danish hygge</title>
    <meta name="copyright" content="Runewood is a company wokring with high quality cases"/>
    <meta name="description" content="Runewood is aiming to deliver a product to bring back 'hygge' in your home." />
    <meta name="keywords" content="Runewood,bar,viking runes, whisky, alcohol, bloset, glass, fine, beverage, cool, enjoy chilled" />
    <meta name="robots" content="index,follow" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.5/umd/popper.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap-filestyle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom.js') }}"></script>
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
    <link rel="stylesheet" href="{{asset("/css/summernote-bs4.css")}}">
    <link rel="stylesheet" href="{{asset("/css/owl.carousel.min.css")}}">
    <link rel="stylesheet" href="{{asset("/css/slider.css")}}">
    <script type="text/javascript" src="{{asset("/js/summernote-bs4.js")}}"></script>
    <script type="text/javascript" src="{{asset("/js/owl.carousel.min.js")}}"></script>

</head>
<body>

<nav class="navbar navbar-dark navbar-expand-md justify-content-between">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-nav">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse dual-nav w-50 order-1 order-md-0">
            <ul class="navbar-nav">
                <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                    <a class="nav-link" href="/">Home <span class="sr-only">Home</span></a>
                </li>

                <li class="dropdown product-dropdown nav-link">
          <a href="#" class="dropdown-toggle nav-link {{ request()->is('Products/*') ? 'active' : '' }}" data-toggle="dropdown">Products <b class="caret"></b></a>
          <ul class="dropdown-menu">
          {{--@if(auth()->guest())--}}
            {{--<li><a class="nav-link {{ request()->is('products') ? 'active' : '' }}" href="/products">Elixir Vault Series</a></li>--}}
            {{--@elseif(auth()->user()->userlevel == 1)--}}
            <li><a class="nav-link {{ request()->is('Products/Cabinets') ? 'active' : '' }}" href="/Products/Cabinets">Cabinets</a></li>
            <li><a class="nav-link {{ request()->is('Products/Wallhangers') ? 'active' : '' }}"  href="/Products/Wallhangers">Wallhangers</a></li>
          {{--@endif--}}

          </ul>
        </li>
                <li class="nav-item {{ Request::is('news') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ url('/news') }}">News</a>
                </li>
                <li class="nav-item {{ Request::is('about') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ url('/about') }}">About</a>
                </li>
                <li class="nav-item {{ Request::is('message/create') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ url('/message/create') }}">Contact</a>
                </li>
                @if(auth()->guest())
                @elseif(auth()->user()->userlevel == 1)
                            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if(Request::is('message'))
                    Contact data
                    @elseif(Request::is('orderdata'))
                    Order data
                    @elseif(Request::is('addpost'))
                    Add post
                    @elseif(Request::is('Products/Add'))
                    Add product
                    @elseif(Request::is('editgallery'))
                    Edit gallery
                    @else
                    Admin panel
                    @endif
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="nav-link dropdown-item" href="{{ url('/message') }}">Contact data</a>
                    <a class="nav-link dropdown-item"  href="{{ url('/orderdata') }}">Order data</a>
                    <a class="nav-link dropdown-item" href="{{ url('/addpost') }}">Add post</a>
                    <a class="nav-link dropdown-item"  href="/Products/Add">Add product</a>
                    <a class="nav-link dropdown-item"  href="/editgallery">Add & edit gallery</a>
                    <a class="nav-link dropdown-item" href="/addfaq">Add new FAQ</a>
                    <a class="nav-link dropdown-item" href="/addtestimonial">Add new Testimonial</a>
                </div>
            </div>
                @endif
            </ul>

        </div>

        <a href="/" class="navbar-brand mx-auto d-block text-center order-0 order-md-1 w-25">
            <div id="site-logo">
                <img src="{{ asset('/img/logo.png') }}" alt="logo" />
            </div>
        </a>
        <div class="navbar-collapse collapse dual-nav w-50 order-2">
            <ul class="nav navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                {{--<li class="nav-item {{ Request::is('login') ? 'active' : '' }}">--}}
                    {{--<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>--}}
                {{--</li>--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
                {{--</li>--}}
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                    @endguest
                    <a id="shopping-cart" href="{{route('product.shoppingCart')}}"><li><i class="fas fa-shopping-cart"></i><span class="cart_quantity badge badge-pill badge-dark">{{Session::has('cart') ? Session::get('cart')->totalQty : 0}}</span></li></a>
            </ul>
        </div>
    </div>
</nav>
@yield('slider')
@yield('construct')
@yield('about')
<div class="container">
    @yield('content')
</div>
@yield('fullwidth')
@yield('maps')


<!--footer starts from here-->
<footer class="footer">
    <div class="container bottom_border">
        <div class="row">
            <div class=" col-sm-4 col-md col-sm-4  col-12 col">
                <h5 class="headin5_amrc col_white_amrc pt2">Find us</h5>
                <!--headin5_amrc-->
                <p class="mb10">
                    We at Runewood are located in Herning so feel free to contact us.
                </p>
                <p><i class="fa fa-location-arrow"></i> Godthåbsvej 76, 7400 Herning </p>
                <p><i class="fa fa-phone"></i>  +45 71 70 68 95  </p>
                <p><i class="fa fa fa-envelope"></i> nordic@runewood.dk  </p>


            </div>


            <div class=" col-sm-12 col-md  col-12 col">
                <h5 class="headin5_amrc col_white_amrc pt2">Our vision</h5>
                <!--headin5_amrc-->
                <p>
                    In a busy society as today, it is our hope to offer a cool way to relax and enjoy the moment. Having our designs to be more than simple furniture, we aim to create unique experiences and ultimately spreading the Danish <i>hygge</i>. Good stories are priceless, and are best shared with others. Therefore, at Runewood we are ambitious to make designs not only worthy of a story, but also to be the centre of where they are told.
                </p>

            </div>



        </div>
    </div>


    <div class="container">
        <ul class="foote_bottom_ul_amrc">
            <li><a href="/">Home</a></li>
            <li><a href="{{ url('/products') }}">Products</a></li>
            <li><a href="{{ url('/news') }}">News</a></li>
            <li><a href="{{ url('/about') }}">About</a></li>
            <li><a href="{{ url('/message/create') }}">Contact</a></li>
            <li><a href="{{ url('/faq') }}">FAQ</a></li>
        </ul>
        <!--foote_bottom_ul_amrc ends here-->
        <p class="text-center">© Runewood 2019 | Designed by Jonathan Højtoft</p>

        <!--social_footer_ul ends here-->
    </div>

</footer>
</body>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127749241-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-127749241-1');
</script>

</html>