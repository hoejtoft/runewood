@extends('...master')

@section('content')


        <div class="row centered">
            <div class="col-md-12">
                <h1 class="text-center Headline" style="font-size: 40px;">Add new news post</h1>
                <hr>
                @if(count($errors) > 0)

                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                        @endif
                <form class="contactForm" method="post" enctype="multipart/form-data" action="{{url('news')}}">
                    {{csrf_field()}}
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label" for="titleinput"><i class="fa fa-font"></i></label>
                            <input class="form-control floatplacehold" value="{{ old('title') }}" name="title" id="titleinput" placeholder="Enter a title" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label" for="subtitleinput"><i class="fa fa-font"></i></label>
                            <input class="form-control floatplacehold" value="{{ old('subtitle') }}" name="subtitle" id="subtitleinput" placeholder="Enter a subtitle" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label" for="imageupload"><i class="fa fa-envelope"></i></label>
                            <input type="file" name="imagepath" id="imagepath" class="filestyle">
                        </div>
                        <div class="col-md-6 col-sm-12 offset-md-3 offset-lg-3">
                            <img width="100%" id="tempimg" src="" style="display:none;">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label" for="text"><i class="fa fa-align-center"></i> Charaters: <span id="characterCount">200</span></label>
                            <textarea maxlength="300" id="newstext" class="form-control" name="breadtext" placeholder="Enter a text for a news post">{{ old('breadtext') }}</textarea>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label" for="text"><i class="fa fa-align-center"></i></label>
                            <textarea class="form-control" name="readmoretext" placeholder="Enter exceeding text content">{{ old('readmoretext') }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary calltoaction"><i class="fa fa-newspaper" style="margin-right:5px;"></i>Add post</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <div class="row">


    </div>

@endsection