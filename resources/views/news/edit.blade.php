@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <br />
            <h3>Edit Record</h3>
            <br />
            @if(count($errors) > 0)

                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                    @endif
                    <form method="post" enctype="multipart/form-data" action="{{action('NewsController@update', $id)}}">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="PATCH" />
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label"  for="titleinput"><i class="fa fa-font"></i></label>
                                <input class="form-control floatplacehold" value="{{$news->title}}" name="title" id="titleinput" placeholder="Enter a title" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label"  for="subtitleinput"><i class="fa fa-font"></i></label>
                                <input class="form-control floatplacehold" value="{{$news->subtitle}}" name="subtitle" id="subtitleinput" placeholder="Enter a subtitle" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="imageupload"><i class="fa fa-envelope"></i></label>
                                <input type="file" name="imagepath" id="imagepath" class="filestyle">
                                <img width="100%" id="{{"tempimg".$news->id}}" src="" style="display:none;">
                            </div>
                            <div class="col-md-6 col-sm-12 offset-md-3 offset-lg-3">
                                <img width="100%" src="{{ asset('/img/uploads/news/image/'.$news->imagepath) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="text"><i class="fa fa-align-center"></i></label>
                                <textarea maxlength="300" class="form-control" id="newstext"  name="breadtext" placeholder="Enter a text for a news post">{{$news->breadtext}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="text"><i class="fa fa-align-center"></i></label>
                                <textarea class="form-control" id="editreadmore" name="readmoretext" placeholder="Enter exceeding text content">{{$news->readmoretext}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="text-center">
                                <a class="btn btn-warning" href="javascript:history.back()"><i class="fa fa-chevron-left" style="margin-right:5px;"></i>Cancel</a>
                                <button type="submit" class="btn btn-primary calltoaction"><i class="fa fa-newspaper" style="margin-right:5px;"></i>Update post</button>

                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </div>

@endsection