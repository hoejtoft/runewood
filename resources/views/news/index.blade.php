@extends('...master')

@section('content')


        <div class="row centered">
            <div class="col-md-12">
                <h1 class="text-center Headline" style="font-size: 40px;">News</h1>
                @if(count($errors) > 0)

                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                        @endif
            </div>
        </div>
    <div class="row">
        @foreach($news as $row)
        <div class="post dropeffect">
                    <div class="post-left">
                        <div class="news-title">
                            <div class="post-left_title">{{$row->title}}</div>
                            <div class="post-left_title_sub">{{$row->subtitle}}</div>
                        </div>
                        <div class="post-left_border"></div>
                        <img src="{{ asset('/img/uploads/news/image/'.$row->imagepath) }}">
                    </div>
                    <div class="post-right">
                        <div class="post-right_body">
                            <p>{{$row->breadtext}}</p>
                            @if($row->readmoretext != "")
                            <div id="text_{{$row->id}}" style="display:none">
                                <p>{{$row->readmoretext}}</p>
                            </div>
                            <a id="toggle_{{$row->id}}" class="btn btn-primary calltoaction nav-toggle">Read More</a>
                            @endif

                        </div>

                        <div class="post-right_footer">
                            <div class="post-right_footer_date">
                                <p><span id="date_title">Posted: </span>{{date("Y-m-d H:i:s", strtotime('+2 hours',strtotime($row->created_at)))}}</p>
                            </div>


                        </div>
                    </div>
            @if(auth()->guest())
                @elseif(auth()->user()->userlevel == 1)
<div class="crud-blok">
                <a>
                <form  method="post" class="delete_form reset-this" action="{{action('NewsController@destroy', $row->id)}}">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="DELETE" />

                                <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                            </form>
                </a>

                <a class="btn btn-warning admincontrol" id="delete_{{$row->id}}" href="{{action('NewsController@edit',$row->id)}}"><i class="fa fa-edit"></i>Edit</a>
            </div>
                @endif
                </div>
        @endforeach

    </div>
    <div id="to-top">t</div>

@endsection

