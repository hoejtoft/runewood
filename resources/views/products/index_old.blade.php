@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <br/>
            <h3 style="text-align: center" >Elixir Vault Series</h3>
            <br/>
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{\Session::get('success')}}</p>
                </div>
            @endif
            <div class="product-form dropeffect">
                <img src="{{ asset('/img/uploads/news/vali.jpg') }}">



                <form class="contactForm" method="post" action="{{url('products')}}">
                    <div class="form-group">
                        <h4 class="product-text">Vali</h4>
                        <hr>
                        <p> "In the west Rind will give birth to Vali.
                            Merely one night old he will avenge the son of Odin.
                            He will not wash his hands, nor will he comb his hair until Balder’s murderer burns at the stake."
                            (Only then will he sit and enjoy a dram of the finest elixir the land has to offer.)
                            <br>
                            <br>
                            Vali, the first in the Elixir Vault Series, is your unique display cabinet, which serves as a lockable,
                            and automatically opening mini bar.
                        </p>
                        <table class="responsive-table fixed-layout">
                            <caption>Product detail</caption>
                            <tbody>
                            <tr>
                                <th scope="row">Access</th>
                                <td>Automatic wireless opening via. app or by direct push button</td>
                            </tr>
                            <tr>
                                <th scope="row">Security</th>
                                <td>Lockable to protect the finest in your collection</td>
                            </tr>
                            <tr>
                                <th scope="row">Lighting</th>
                                <td>Dimmable internal lighting</td>
                            </tr>
                            <tr>
                                <th scope="row">Mounting</th>
                                <td>Can be wall mounted or self-standing</td>
                            </tr>
                            <tr>
                                <th scope="row">Capacity</th>
                                <td>Houses up to 15 fine spirits, and 12 glasses</td>
                            </tr>
                            <tr>
                                <th scope="row">Dimensions</th>
                                <td>Length: 105cm Height: 44cm Depth: 24cm</td>
                            </tr>
                            <tr>
                                <th scope="row">Price</th>
                                <td>18.399 DKK</td>
                            </tr>
                            </tbody>
                        </table>
                        <p>Please contact our office for a showing, we will be delighted to give you a demonstration.</p>
                    </div>
                    {{csrf_field()}}
                    <div class="form-group">
                        <input type="hidden" name="country" class="form-control" placeholder="Enter country" />
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label" for="nameinput"><i class="fa fa-user"></i></label>
                            <input class="form-control" name="first_name" id="nameinput" placeholder="Enter your name" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label" for="surnameinput"><i class="fa fa-user"></i></label>
                            <input class="form-control" name="last_name" id="surnameinput" placeholder="Enter your last name" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label" for="adressinput"><i class="fa fa-map-marker-alt"></i></label>
                            <input class="form-control" name="address_street" id="addressinput" placeholder="Enter your address" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label" for="postalinput"><i class="fa fa-location-arrow"></i></label>
                            <input class="form-control" name="address_postal" id="postalinput" placeholder="Enter your postal code" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label" for="cityinput"><i class="fa fa-university"></i></label>
                            <input class="form-control" name="address_city" id="cityinput" placeholder="Enter your city" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label" for="phoneinput"><i class="fa fa-mobile-alt"></i></label>
                            <input class="form-control" name="phone" id="addressinput" placeholder="Enter your phonenumber" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label" for="mailinput"><i class="fa fa-envelope"></i></label>
                            <input class="form-control" name="email" id="mailinput" placeholder="Enter your mail" type="email">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="text-center">
                            <input type="submit" value="Place your order" class="btn btn-primary calltoaction" />
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection