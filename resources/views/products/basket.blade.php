@extends('master')

@section('content')
    <h3 style="text-align: center">Basket</h3>
    <div class="row">
        <table class="table basket_table table-responsive dropeffect">
            <thead>
            <tr>
                <th>Product</th>
                <th>Category</th>
                <th>Quantity</th>
                <th class="text-center">Price</th>
                <th class="text-center">Total</th>
                <th> Delete </th>
            </tr>
            </thead>
            <tbody>
            @if(Session::has('cart'))

                @foreach($products as $product)
                    <tr>
                        <td class="col-md-6">
                            <div class="media">
                                <span class="thumbnail pull-left"><img class="media-object" src="{{ asset('/img/uploads/news/image/'.$product['item']['img1']) }}" style="width: 50%;"> </span>
                                <div class="media-body">
                                    <h4 class="media-heading"><span class="product-heading">{{$product['item']['productname']}}</span></h4>
                                    <h5 class="media-heading designer">by {{$product['item']['author']}}</h5>

                                </div>
                            </div>
                        </td>
                        <td class="col-md-1">
                            {{$product['item']['category']}}
                        </td>
                        <td class="col-md-2" style="text-align: center">
                            <input type="text" class="form-control cQuantityInput numqty " id="QuantityInput" value="{{$product['qty']}}">
                            <a href="{{route('product.updateProduct',['id' => $product['item']['id'],'qty' => 0 ])}}" class="numsubtract"><i class="far fa-minus-square"></i></a>
                            <a href="{{route('product.updateProduct',['id' => $product['item']['id'],'qty' => 1 ])}}" class=" numadd"><i class="far fa-plus-square"></i></a>
                        </td>
                        <td class="col-md-1 text-center"><span  id="productprice_0">{{$product['item']['price']}}</span> DKK</td>
                        <td class="col-md-1 text-center"><span  id="totalproductprice_0">0.00</span> DKK</td>
                        <td class="col-md-1">
                            <a class="btn btn-danger" href="{{route('product.deleteProduct',['id' => $product['item']['id'],'qty' =>$product['qty'] ])}}">
                                <i class="fas fa-times"></i>
                            </a></td>
                    </tr>
                    @endforeach


                @else
                <tr>
                    <td class="col-md-2">

                    </td>
                    <td class="col-md-1">

                    </td>
                    <td class="col-md-6" style="text-align: center">
                        No items in your cart
                    </td>

                    <td class="col-md-1 text-center"></td>
                    <td class="col-md-1 text-center"></td>
                    <td class="col-md-1"></td>
                </tr>
                @endif

            <tr>
                <td colspan="6" class="text-right "><p class="Subtotalprice">Subtotal</p><span class="Subtotalprice" id="Subtotal_price">0.00 </span>DKK</td>
            </tr>
            <tr>
                <td colspan="6" class="text-right "><p class="Shippingprice">Shipping</p><span class="Shippingprice" id="Shipping_price">25.00 </span>DKK</td>
            </tr>
            <tr>
                <td colspan="6" class="text-right "><p class="Totalprice">Total</p><span class="Totalprice" id="total_price">0.00 </span>DKK</td>
            </tr>
            <tr>
                <td>   </td>
                <td>   </td>
                <td>   </td>
                <td>   </td>
                <td><a class="btn btn-danger float-right calltoaction" href="{{route('product.resetCart')}}"> Reset cart
                        <i class="fas fa-times"></i>
                    </a>    </td>

                <td>
                    <a type="button" href="/checkout" class="btn btn-success float-right">
                        Go to checkout <i class="fas fa-credit-card"></i>
                    </a></td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection