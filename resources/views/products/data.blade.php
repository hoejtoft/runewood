@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3 align="center">Order Data</h3>
            <br />
            @if(\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{\Session::get('success')}}</p>
                </div>
            @endif
            <table class="table table-bordered table-striped custom-table-responsive">
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Address</th>
                    <th>Postal code</th>
                    <th>City</th>
                    <th>Country</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Created at:</th>
                    <th>Delete</th>
                </tr>
                @foreach($order as $row)
                    <tr>
                        <td>{{$row['first_name']}}</td>
                        <td>{{$row['last_name']}}</td>
                        <td>{{$row['address']}}</td>
                        <td>{{$row['postal_code']}}</td>
                        <td>{{$row['city']}}</td>
                        <td>{{$row['country']}}</td>
                        <td>{{$row['phone']}}</td>
                        <td>{{$row['email']}}</td>
                        <td>{{$row['created_at']}}</td>
                        <td>
                            <form  method="post" class="delete_form reset-this" action="{{action('OrderController@destroy', $row['id'])}}">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="DELETE" />
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection