@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <br />
            <h3>Edit Record</h3>
            <br />
            @if(count($errors) > 0)

                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                    @endif
                    <form method="post" enctype="multipart/form-data" action="{{action('ProductController@update', $id)}}">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="PATCH" />
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="productname"><i class="fa fa-font"></i></label>
                                <input class="form-control floatplacehold"  value="{{ $product->productname }}" name="productname" id="productname" placeholder="Product name" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="category"><i class="fa fa-font"></i></label>
                                <select name="category" class="form-control selectpicker">
                                    <option>Select a category</option>
                                    <option {{$product->category == "Cabinet" ? "selected" : ""}}>Cabinet</option>
                                    <option {{$product->category == "Wallhanger" ? "selected" : ""}}>Wallhanger</option>
                                </select>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="author"><i class="fa fa-font"></i></label>
                                <input class="form-control floatplacehold" value="{{ $product->author}}" name="author" id="author" placeholder="Enter a author" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row no-margin">
                                <div class="col-md-8 offset-md-2">
                                    <label class="control-label" for="imageupload"><i class="fa fa-envelope"></i></label>
                                    <input type="file" name="imagepath1" id="imagepath1" class="filestyle">
                                    <img width="100%" id="tempimg1" src="" style="display:none;">
                                    <div class="editimg" id="editimg_1">
                                        <img src="{{ asset('/img/uploads/news/image/'.$product->img1) }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row no-margin">

                                <div class="col-md-6">
                                    <label class="control-label" for="imageupload"><i class="fa fa-envelope"></i></label>
                                    <input type="file" name="imagepath2" id="imagepath2" class="filestyle">
                                    <img width="100%" id="tempimg2" src="" style="display:none;">
                                    <div class="editimg" id="editimg_2">
                                        <img src="{{ asset('/img/uploads/news/image/'.$product->img2) }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label" for="imageupload"><i class="fa fa-envelope"></i></label>
                                    <input type="file" name="imagepath3" id="imagepath3" class="filestyle">
                                    <img width="100%" id="tempimg3" src="" style="display:none;">
                                    <div class="editimg" id="editimg_3">
                                        <img src="{{ asset('/img/uploads/news/image/'.$product->img3) }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row no-margin">

                                <div class="col-md-6">
                                    <label class="control-label" for="imageupload"><i class="fa fa-envelope"></i></label>
                                    <input type="file" name="imagepath4" id="imagepath4" class="filestyle">
                                    <img width="100%" id="tempimg4" src="" style="display:none;">
                                    <div class="editimg" id="editimg_4">
                                        <img src="{{ asset('/img/uploads/news/image/'.$product->img4) }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label" for="imageupload"><i class="fa fa-envelope"></i></label>
                                    <input type="file" name="imagepath5" id="imagepath5" class="filestyle">
                                    <img width="100%" id="tempimg5" src="" style="display:none;">
                                    <div class="editimg" id="editimg_5">
                                        <img src="{{ asset('/img/uploads/news/image/'.$product->img5) }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="description"><i class="fa fa-align-center"></i></label>
                                <textarea class="form-control" maxlength="500" id="wysiwyg" name="description" placeholder="Add a product description">{{ $product->description }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="amount"><i class="fa fa-font"></i></label>
                                <input class="form-control floatplacehold" value="{{ $product->amount }}" name="amount" id="amount" placeholder="Enter amount of products" type="text">
                            </div>
                        </div>



                        <hr>
                        <h2>Product details</h2>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="price"><i class="fa fa-font"></i></label>
                                <input class="form-control floatplacehold" value="{{ $product->price }}" name="price" id="price" placeholder="Enter a price" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="materials"><i class="fa fa-font"></i></label>
                                <input class="form-control floatplacehold" value="{{ $product->materials}}" name="materials" id="materials" placeholder="Enter materials" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="finish"><i class="fa fa-font"></i></label>
                                <input class="form-control floatplacehold" value="{{ $product->finish }}" name="finish" id="finish" placeholder="Enter finish" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="capacity"><i class="fa fa-font"></i></label>
                                <input class="form-control floatplacehold" value="{{ $product->capacity }}" name="capacity" id="capacity" placeholder="Enter capacity" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="lighting"><i class="fa fa-font"></i></label>
                                <input class="form-control floatplacehold" value="{{ $product->lighting }}" name="lighting" id="lighting" placeholder="Enter a lighting setting" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="features"><i class="fa fa-font"></i></label>
                                <input class="form-control floatplacehold" value="{{ $product->features }}" name="features" id="features" placeholder="Enter features" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="warranty"><i class="fa fa-font"></i></label>
                                <input class="form-control floatplacehold" value="{{ $product->warranty }}" name="warranty" id="warranty" placeholder="Enter a warranty policy" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="heigth"><i class="fa fa-font"></i></label>
                                <input class="form-control floatplacehold" value="{{ $product->heigth }}" name="heigth" id="heigth" placeholder="Enter heigth" type="text">
                            </div>
                        </div>
                        {{--Lenght = Width--}}
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="length"><i class="fa fa-font"></i></label>
                                <input class="form-control floatplacehold" value="{{ $product->length }}" name="length" id="length" placeholder="Enter width" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label" for="depth"><i class="fa fa-font"></i></label>
                                <input class="form-control floatplacehold" value="{{ $product->depth }}" name="depth" id="depth" placeholder="Enter depth" type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="text-center">
                                <a class="btn btn-warning" href="javascript:history.back()"><i class="fa fa-chevron-left" style="margin-right:5px;"></i>Cancel</a>
                                <button type="submit" class="btn btn-primary calltoaction"><i class="fa fa-newspaper" style="margin-right:5px;"></i>Update product</button>

                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </div>

@endsection