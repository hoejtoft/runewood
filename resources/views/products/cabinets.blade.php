@extends('master')

@section('content')
    <h3 style="text-align: center" >Cabinets</h3>
    <div class="row content_container">



        @foreach($product as $row)
        <div class="col-md-6">
            <div class="Product-box dropeffect">
                <div class="left-product"><img src="{{ asset('/img/uploads/news/image/'.$row->img1) }}">

                </div>
                <div class="right-product">
                    <h5>{{$row->productname}}</h5>
                    <h6>{{$row->category}}</h6>
                    <h4 id="products_price">{{$row->price}} DKK</h4>
                    <hr>
                    <p>Designer: {{$row->author}}</p>
                    <p>{!!$row->description!!}</p>
                    <br>
                    <div id="button-box">

                                <a href="{{route('product.addToCart',['id' => $row->id,'qty' => 1])}}" class="btn btn-danger calltoaction halfwidth">Add to cart <i class="fas fa-cart-plus"></i></a>


                                <button class="btn btn-primary info-btn" data-toggle="modal" data-target="#product_view{{$row->id}}">More info <i class="fas fa-info-circle"></i></button>

                    </div>
                </div>
                            @if(auth()->guest())
                @elseif(auth()->user()->userlevel == 1)
<div class="crud-blok">
                <a>
                <form  method="post" class="delete_form reset-this" action="{{action('ProductController@destroy', $row->id)}}">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="DELETE" />

                                <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                            </form>
                </a>

                <a class="btn btn-warning admincontrol" id="delete_{{$row->id}}" href="{{action('ProductController@edit',$row->id)}}"><i class="fa fa-edit"></i>Edit</a>
            </div>
                @endif
            </div>
        </div>

            {{--Modal markup--}}
            <div class="modal fade product_view" id="product_view{{$row->id}}">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <a href="#" data-dismiss="modal" class="class pull-right"><i class="fas fa-times"></i></a>
                            <h3 class="modal-title">{{$row->productname}}</h3>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 product_img">

                                    {{--Modal images--}}
                                    <div class="container">

                                        <!-- main slider carousel -->
                                        <div class="row">
                                            <div class="col-md-12 productSlider" id="slider">
                                                <div id="myCarousel{{$row->id}}" class="carousel slide">
                                                    <!-- main slider carousel items -->
                                                    <div class="carousel-inner">


                                                        @if($row->img1 != "")
                                                                <div class="item carousel-item first_slide_1" data-slide-number="1">
                                                                    <img src="{{ asset('/img/uploads/news/image/'.$row->img1) }}" class="img-fluid">
                                                                </div>

                                                        @endif
                                                        @if($row->img2 != "")
                                                                <div class="item carousel-item first_slide_2" data-slide-number="2">
                                                                    <img src="{{ asset('/img/uploads/news/image/'.$row->img2) }}" class="img-fluid">
                                                                </div>
                                                        @endif
                                                        @if($row->img3 != "")
                                                                <div class="item carousel-item first_slide_3" data-slide-number="3">
                                                                    <img src="{{ asset('/img/uploads/news/image/'.$row->img3) }}" class="img-fluid">
                                                                </div>
                                                        @endif
                                                        @if($row->img4 != "")
                                                                <div class="item carousel-item first_slide_4" data-slide-number="4">
                                                                    <img src="{{ asset('/img/uploads/news/image/'.$row->img4) }}" class="img-fluid">
                                                                </div>
                                                        @endif
                                                        @if($row->img5 != "")
                                                                <div class="item carousel-item first_slide_5" data-slide-number="5">
                                                                    <img src="{{ asset('/img/uploads/news/image/'.$row->img5) }}" class="img-fluid">
                                                                </div>
                                                        @endif


                                                        <a class="carousel-control float-left pt-3" href="#myCarousel{{$row->id}}" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
                                                        <a class="carousel-control float-right right pt-3" href="#myCarousel{{$row->id}}" data-slide="next"><i class="fa fa-chevron-right"></i></a>

                                                    </div>
                                                    <!-- main slider carousel nav controls -->


                                                    <ul class="carousel-indicators list-inline">

                                                            @if($row->img1 != "")
                                                                <li class="list-inline-item first_slide_1" data-slide-to="0" data-target="#myCarousel{{$row->id}}">
                                                                    <a id="carousel-selector-1" class="selected" >
                                                                        <img src="{{ asset('/img/uploads/news/image/'.$row->img1) }}" width="80px" height="60px" class="img-fluid">
                                                                    </a>
                                                                </li>

                                                            @endif
                                                            @if($row->img2 != "")
                                                                    <li class="list-inline-item first_slide_2" data-slide-to="1" data-target="#myCarousel{{$row->id}}">
                                                                        <a id="carousel-selector-2"  >
                                                                            <img src="{{ asset('/img/uploads/news/image/'.$row->img2) }}" width="80px" height="60px" class="img-fluid">
                                                                        </a>
                                                                    </li>
                                                            @endif
                                                            @if($row->img3 != "")
                                                                    <li class="list-inline-item first_slide_3" data-slide-to="2" data-target="#myCarousel{{$row->id}}">
                                                                        <a id="carousel-selector-3" >
                                                                            <img src="{{ asset('/img/uploads/news/image/'.$row->img3) }}" width="80px" height="60px" class="img-fluid">
                                                                        </a>
                                                                    </li>
                                                            @endif
                                                            @if($row->img4 != "")
                                                                    <li class="list-inline-item first_slide_4" data-slide-to="3" data-target="#myCarousel{{$row->id}}">
                                                                        <a id="carousel-selector-4"  >
                                                                            <img src="{{ asset('/img/uploads/news/image/'.$row->img4) }}" width="80px" height="60px" class="img-fluid">
                                                                        </a>
                                                                    </li>
                                                            @endif
                                                            @if($row->img5 != "")
                                                                    <li class="list-inline-item first_slide_5" data-slide-to="4" data-target="#myCarousel{{$row->id}}">
                                                                        <a id="carousel-selector-5" >
                                                                            <img src="{{ asset('/img/uploads/news/image/'.$row->img5) }}" width="80px" height="60px" class="img-fluid">
                                                                        </a>
                                                                    </li>
                                                            @endif

                                                    </ul>
                                                </div>
                                            </div>

                                        </div>
                                        <!--/main slider carousel-->
                                    </div>
                                    {{--Modal images--}}
                                </div>
                                <div class="col-md-6 product_content">

                                    <p>{!!$row->description!!}</p>
                                    <table class="responsive-table fixed-layout">
                                        <caption>Product detail</caption>
                                        <tbody>
                                        @if($row->materials != "" || $row->materials != NULL)
                                        <tr>
                                            <th scope="row">Materials</th>
                                            <td>{{$row->materials}}</td>
                                        </tr>
                                        @endif
                                        @if($row->finish != "" || $row->finish != NULL)
                                        <tr>
                                            <th scope="row">Finish</th>
                                            <td>{{$row->finish}}</td>
                                        </tr>
                                        @endif
                                        @if($row->capacity != "" || $row->capacity != NULL)
                                        <tr>
                                            <th scope="row">Capacity</th>
                                            <td>{{$row->capacity}}</td>
                                        </tr>
                                        @endif
                                        @if($row->lighting != "" || $row->lighting != NULL)
                                        <tr>
                                            <th scope="row">Lighting</th>
                                            <td>{{$row->lighting}}</td>
                                        </tr>
                                        @endif
                                        @if($row->features != "" || $row->features != NULL)
                                        <tr>
                                            <th scope="row">Features</th>
                                            <td>{{$row->features}}</td>
                                        </tr>
                                        @endif
                                        @if($row->warranty != "" || $row->warranty != NULL)
                                        <tr>
                                            <th scope="row">Warranty</th>
                                            <td>{{$row->warranty}}</td>
                                        </tr>
                                        @endif
                                        <tr>
                                            <th scope="row">Dimensions</th>
                                            <td>Height: {{$row->heigth}}cm Width: {{$row->length}}cm Depth: {{$row->depth}}cm</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <h3 class="cost" id="products_price"> {{$row->price}} DKK</h3>
                                    <div class="row">

                                        <div class="col-md-6 col-sm-12">
                                            <select class="form-control amountSelected" name="select">
                                                <option value="" selected="">Amount</option>
                                                @for($i=1;$i<$row->amount+1;$i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="btn-ground">
                                                <a href="{{route('product.addToCart',['id' => $row->id,'qty' => 1])}}" class="btn btn-primary calltoaction btn-fullwidth qtyadd"><span class="glyphicon glyphicon-shopping-cart"></span> Add To Cart <i class="fas fa-cart-plus"></i></a>

                                            </div>
                                        </div>
                                        <!-- end col -->
                                    </div>
                                    <div class="space-ten"></div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--Modal Markup--}}
        {{--@endfor--}}
        @endforeach
    </div>



@endsection
{{--@section('fullwidth')--}}
{{--@if(auth()->guest())--}}
{{--@elseif(auth()->user()->userlevel == 1)--}}
    {{--<div class="contentmaxwidth fullwidth-bg">--}}

        {{--<section class="reviews">--}}
            {{--<h4 class="main-title text-center font-weight-bold pb-4">WHAT CUSTOMERS SAY</h4>--}}

            {{--<div class="container">--}}
                {{--<div class="owl-carousel">--}}

                    {{--<!-- card -->--}}
                    {{--@foreach($testimonial as $row)--}}

                        {{--<div class="item">--}}
                            {{--<div class="card dropeffect">--}}
                                {{--<div class="row">--}}
                                {{--<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 p-col">--}}
                                        {{--<p class="text-center">{{$row->testimonial_text}}</p>--}}
                                {{--</div>--}}

                                    {{--<div class="col-md-12 info-col">--}}
                                        {{--<h4 class="title text-center">{{$row->testimonial_name}}</h4>--}}
                                        {{--<h6 class="sub-title text-center">{{$row->testimonial_profession}}</h6>--}}
                                        {{--<ul>--}}
                                            {{--@if($row->testimonial_rating == 0 || $row->testimonial_rating == null)--}}
                                            {{--@elseif($row->testimonial_rating == 1)--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--@elseif($row->testimonial_rating == 2)--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--@elseif($row->testimonial_rating == 3)--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--@elseif($row->testimonial_rating == 4)--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--@elseif($row->testimonial_rating == 5)--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--<li><i class="fa fa-star"></i></li>--}}
                                            {{--@endif--}}

                                        {{--</ul>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 img-col">--}}
                                        {{--<div class="user" style="background-image: url('{{$row->testimonial_image}}')"></div>--}}
                                    {{--</div>--}}


                                {{--</div>--}}
                            {{--</div>--}}
                {{--@if(auth()->guest())--}}
                {{--@elseif(auth()->user()->userlevel == 1)--}}
                {{--<div class="crud-blok testimonial-crud">--}}
                {{--<a>--}}
                {{--<form  method="post" class="delete_form reset-this" action="{{action('testimonialController@destroy', $row->id)}}">--}}
                                {{--{{csrf_field()}}--}}
                                {{--<input type="hidden" name="_method" value="DELETE" />--}}

                                {{--<button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>--}}
                            {{--</form>--}}
                {{--</a>--}}

                {{--<a class="btn btn-warning admincontrol" id="delete_{{$row->id}}" href="{{action('testimonialController@edit',$row->id)}}"><i class="fa fa-edit"></i>Edit</a>--}}
            {{--</div>--}}
                {{--@endif--}}
                        {{--</div>--}}
                    {{--@endforeach--}}







                {{--</div>--}}
            {{--</div>--}}
        {{--</section>--}}
    {{--</div>--}}
    {{--@endif--}}
{{--@endsection--}}