@extends('master')

@section('content')
    <h3 style="text-align: center">Checkout</h3>
    <div class="row nomalize-row">
        @if(count($errors) > 0)

            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
                @endif
            </div>
        @if(session()->has('success'))
                <div id="myModal" class="modal fade">
                    <div class="modal-dialog modal-confirm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="icon-box">
                                    <i class="fas fa-check"></i>
                                </div>
                                <h4 class="modal-title">Awesome!</h4>
                            </div>
                            <div class="modal-body">
                                <p class="text-center">Your order has been placed. Check your email for details.</p>
                            </div>
                            <div class="modal-footer">
                                <button id="checkout-success" class="btn btn-success btn-block" data-dismiss="modal">OK</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center" style="display:none">
                    <!-- Button HTML (to Trigger Modal) -->
                    <a href="#myModal"  id="trigger-btn" data-toggle="modal">Click to Open Confirm Modal</a>
                </div>
            <script>
                //$('#myModal').modal('show');
                setTimeout(function(){
                    $("#trigger-btn").trigger("click");
                }, 1000);

            </script>
            @endif
        <form class="contactForm" method="post" action="{{url('products')}}">
            {{csrf_field()}}
            <hr>
            <h2>Shipping information</h2>
            @if(Session::has('cart'))
            <table class="responsive-table fixed-layout">
                                        <caption>Products</caption>
                                        <tbody>
                                        @foreach($products as $product)
                                            <tr>
                                                <th scope="row"> {{$product['qty']}} x {{$product['item']['productname']}}</th>
                                                <td> <span class="thumbnail pull-left"><img class="media-object" src="{{ asset('/img/uploads/news/image/'.$product['item']['img1']) }}" style="width: 50%;"> </span></td>

                                            </tr>
                                        @endforeach
                                        <?php
                                        $cart = array();
                                        foreach($products as $product){
                                        array_push($cart,$product['qty']. " x " .$product['item']['productname'].",".$product['item']['price']);
                                        }
                                        ?>


                                        <input id="productdetails" type="hidden" value="{{implode("|",$cart)}}" name="productdetails">
                                        </tbody>
                                    </table>
                @else
            @endif

            <div class="form-group">
                <div class="col-sm-12 col-md-12">
                    <label class="control-label" for="Invoice_FirstName"><i class="fa fa-font"></i></label>
                    <input class="form-control floatplacehold" value="{{ old('Invoice_Name') }}" name="Invoice_Name" id="Invoice_Name" placeholder="Enter your first name" type="text">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12 col-md-12">
                    <label class="control-label" for="Invoice_LastName"><i class="fa fa-font"></i></label>
                    <input class="form-control floatplacehold" value="{{ old('Invoice_LastName') }}" name="Invoice_LastName" id="Invoice_LastName" placeholder="Enter your last name" type="text">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12 col-md-12">
                    <label class="control-label" for="Invoice_LastName"><i class="fa fa-font"></i></label>
                    <input class="form-control floatplacehold" value="{{ old('Invoice_Phone') }}" name="Invoice_Phone" id="Invoice_Phone" placeholder="Enter your phone number" type="text">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12 col-md-12">
                    <label class="control-label" for="Invoice_Email"><i class="fa fa-font"></i></label>
                    <input class="form-control floatplacehold" value="{{ old('Invoice_Email') }}" name="Invoice_Email" id="Invoice_Email" placeholder="Enter your email" type="text">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12 col-md-12">
                    <label class="control-label" for="Invoice_cEmail"><i class="fa fa-font"></i></label>
                    <input class="form-control floatplacehold" value="{{ old('Invoice_cEmail') }}" name="Invoice_cEmail" id="Invoice_cEmail" placeholder="Confirm your email" type="text">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12 col-md-12">
                    <label class="control-label" for="Invoice_Address"><i class="fa fa-font"></i></label>
                    <input class="form-control floatplacehold" value="{{ old('Invoice_Address') }}" name="Invoice_Address" id="Invoice_Address" placeholder="Enter your shipping address" type="text">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12 col-md-12">
                    <label class="control-label" for="Invoice_Postal"><i class="fa fa-font"></i></label>
                    <input class="form-control floatplacehold" value="{{ old('Invoice_Postal') }}" name="Invoice_Postal" id="Invoice_Postal" placeholder="Enter your postal code" type="text">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12 col-md-12">
                    <label class="control-label" for="Invoice_City"><i class="fa fa-font"></i></label>
                    <input class="form-control floatplacehold" value="{{ old('Invoice_City') }}" name="Invoice_City" id="Invoice_City" placeholder="Enter city" type="text">
                </div>
            </div>
            <div class="form-group">
                <input type="hidden" name="country" class="form-control" placeholder="Enter country" />
            </div>


            <hr>

            <div class="paymentCont">
                <div class="headingWrap">
                    <h3 class="headingTop text-center">Select Your Payment Method</h3>
                </div>
                <div class="paymentWrap">
                    <div class="btn-group paymentBtnGroup btn-group-justified" data-toggle="buttons">
                        <label class="btn paymentMethod active">
                            <div class="method invoice"></div>
                            <input type="radio" name="options" checked>
                        </label>
                        {{--<label class="btn paymentMethod">--}}
                            {{--<div class="method visa"></div>--}}
                            {{--<input type="radio" name="options">--}}
                        {{--</label>--}}
                        {{--<label class="btn paymentMethod">--}}
                            {{--<div class="method master-card"></div>--}}
                            {{--<input type="radio" name="options">--}}
                        {{--</label>--}}
                        {{--<label class="btn paymentMethod">--}}
                            {{--<div class="method mobilepay"></div>--}}
                            {{--<input type="radio" name="options">--}}
                        {{--</label>--}}


                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="text-center">
                    <a class="btn btn-danger" href="/Basket"><i class="fas fa-arrow-left" style="margin-right:5px;"></i>Back to basket</a>
                    <button type="submit" class="btn btn-success"><i class="fa fa-newspaper" style="margin-right:5px;"></i>Place order</button>
                </div>
            </div>
        </form>
    </div>
@endsection