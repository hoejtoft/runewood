@extends('master')

@section('content')
    <div class="container vhcontainer">
        <div class="row centered">
            <div class="col-md-12">
                <h1 class="text-center Headline" style="font-size: 40px;">FAQ</h1>
            </div>
        </div>
        <section class="faq-section">
            <div class="container">
                <div class="row">

                    <!-- ***** FAQ Start ***** -->
                    <div class="col-md-12">
                        <div class="faq" id="accordion">
                            @foreach($faq as $row)
                                <div class="card">
                                <div class="card-header" id="faqHeading-{{$row['id']}}">
                                    <div class="mb-0">
                                        <h5 class="faq-title" data-toggle="collapse" data-target="#faqCollapse-{{$row['id']}}" data-aria-expanded="true" data-aria-controls="faqCollapse-{{$row['id']}}">
                                            <span class="badge"><i class="fas fa-question"></i></span>{{$row['Question']}}
                                        </h5>
                                    </div>
                                </div>
                                <div id="faqCollapse-{{$row['id']}}" class="collapse" aria-labelledby="faqHeading-{{$row['id']}}" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>{{$row['Answer']}} </p>
                                    </div>
                                </div>
                            @if(auth()->guest())
                            @elseif(auth()->user()->userlevel == 1)
                            <div class="crud-blok crud-faq">
                                <a>
                                    <form  method="post" class="delete_form reset-this" action="{{action('FAQController@destroy', $row['id'])}}">
                                        {{csrf_field()}}
                                        <input type="hidden" name="_method" value="DELETE" />
                                        <button type="submit" class="btn btn-danger admincontrol"><i class="fa fa-times"></i> Delete</button>
                                    </form>
                                </a>
                                <a class="btn btn-warning admincontrol" id="delete_{{$row['id']}}" href="{{action('FAQController@edit',$row['id'])}}"><i class="fa fa-edit"></i>Edit</a>
                            </div>
                            @endif
                            </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection