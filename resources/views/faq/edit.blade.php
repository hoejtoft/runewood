@extends('...master')

@section('content')

        <div class="vhcontainer">
        <div class="row centered">
            <div class="col-md-12">
                <h1 class="text-center Headline" style="font-size: 40px;">Edit faq</h1>
                <hr>
                @if(count($errors) > 0)

                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>

                    </div>
                @endif
                <form class="contactForm" method="post" enctype="multipart/form-data" action="{{action('FAQController@update', $id)}}">
                    {{csrf_field()}}
                    {{ method_field('PUT')}}
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label" for="Question"><i class="fa fa-font"></i></label>
                            <input class="form-control floatplacehold" value="{{ $faq->Question }}" name="Question" id="Question" placeholder="Enter a question title" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label" for="Answer"><i class="fa fa-font"></i></label>
                            <input class="form-control floatplacehold" value="{{ $faq->Answer }}" name="Answer" id="Answer" placeholder="Enter an answer" type="text">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary calltoaction"><i class="fa fa-newspaper" style="margin-right:5px;"></i>Update faq</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        </div>

@endsection