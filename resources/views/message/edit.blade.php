@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <br />
            <h3>Edit Record</h3>
            <br />
            @if(count($errors) > 0)

                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                    @endif
                    <form method="post" action="{{action('MessageController@update', $id)}}">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="PATCH" />
                        <div class="form-group">
                            <input type="text" name="first_name" class="form-control" value="{{$message->first_name}}" placeholder="Enter First Name" />
                        </div>
                        <div class="form-group">
                            <input type="text" name="email" class="form-control" value="{{$message->email}}" placeholder="Enter email" />
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary calltoaction" value="Edit" />
                        </div>
                    </form>
                </div>
        </div>
    </div>

@endsection