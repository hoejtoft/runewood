@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3 align="center">Message Data</h3>
            <br />
            @if($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{$message}}</p>
                </div>
            @endif
            <table class="table table-bordered table-striped custom-table-responsive">
                <tr>
                    <th>First Name</th>
                    <th>E-mail</th>
                    <th>Country</th>
                    <th>Message</th>
                    <th>Created at:</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                @foreach($messages as $row)
                    <tr>
                        <td>{{$row['first_name']}}</td>
                        <td>{{$row['email']}}</td>
                        <td>{{$row['country']}}</td>
                        <td>{{$row['text']}}</td>
                        <td>{{$row['created_at']}}</td>
                        <td><a class="btn btn-warning" href="{{action('MessageController@edit',$row['id'])}}">Edit</a></td>
                        <td>
                            <form  method="post" class="delete_form reset-this" action="{{action('MessageController@destroy', $row['id'])}}">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="DELETE" />
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection