@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <br/>
            <h3 style="text-align: center" >Have any questions? <br> Write a message to Runewood</h3>
            <br/>
            @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if(\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{\Session::get('success')}}</p>
                </div>
            @endif
            <form class="contactForm" method="post" action="{{url('message')}}">
                {{csrf_field()}}
                <div class="form-group">
                    <input type="hidden" name="country" class="form-control" placeholder="Enter country" />
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="control-label" for="nameinput"><i class="fa fa-user"></i></label>
                        <input class="form-control" name="first_name" id="nameinput" placeholder="Enter your name" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="control-label" for="mailinput"><i class="fa fa-envelope"></i></label>
                        <input class="form-control" name="email" id="mailinput" placeholder="Enter your mail" type="email">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="control-label" for="text"><i class="fa fa-comment"></i></label>
                        <textarea class="form-control" name="text" placeholder="Enter a message"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="text-center">
                     <input type="submit" value="Send message" class="btn btn-primary calltoaction" />
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('maps')
    {{--<iframe src="https://snazzymaps.com/embed/107160" width="100%" height="350px" style="border:none; margin-bottom:-7px;"></iframe>--}}
@endsection