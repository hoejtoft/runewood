<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //protected $table = 'products';
    protected $fillable = [
                            'productname',
                            'category',
                            'author',
                            'price',
                            'amount',
                            'img1',
                            'img2',
                            'img3',
                            'img4',
                            'img5',
                            'description',
                            'length',
                            'heigth',
                            'depth',
                            'capacity',
                            'security',
                            'lighting',
                            'warranty',
                            'materials',
                            'finish',
                            'features'
                         ];
}
