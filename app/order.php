<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    protected $table = 'order';
    protected $fillable = ['first_name', 'last_name','address','postal_code','city','phone','email','country','productdetails'];
}
