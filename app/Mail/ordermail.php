<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ordermail extends Mailable
{
    use Queueable, SerializesModels;
    public $name;
    public $lastname;
    public $address;
    public $postal_code;
    public $city;
    public $phone;
    public $email;
    public $country;
    public $productdetails;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$lastname,$address,$postal_code,$city,$phone,$email,$country,$productdetails)
    {
        $this->name = $name;
        $this->lastname = $lastname;
        $this->address = $address;
        $this->postal_code = $postal_code;
        $this->city = $city;
        $this->phone = $phone;
        $this->email = $email;
        $this->country = $country;
        $this->productdetails = $productdetails;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@runewood.dk')->view('emails.orderdetails');
    }
}
