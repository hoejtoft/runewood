<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class NewsController extends Controller
{
    public function index()
    {
        //$news = News::all()->toArray();
        $news = DB::table('news')->orderByRaw('created_at DESC')->get()->toArray();
        return view('news.index',compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {

            return view('news.addpost');

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'subtitle' => 'required',
            'imagepath' => 'required',
            'breadtext' => 'required'
        ]);

        if($request->hasfile('imagepath'))
        {
            $file = $request->file('imagepath');
            $filename =$request->get('imagepath');
            if(!$path = Storage::disk('public_uploads')->put($filename, $file)) {
                return false;
            }
            $newz = new News([
                'title' => $request->get('title'),
                'subtitle' => $request->get('subtitle'),
                'imagepath' => $path,
                'breadtext' => $request->get('breadtext'),
                'readmoretext' => $request->get('readmoretext')
            ]);
            $newz->save();
            return redirect()->action('NewsController@index');
        }
        else{
            return "false";
        }




//        $request->file('imagepath')->store('uploads');
//        return "uploaded:";
        //return $newz->title .",". $newz->subtitle .",". $newz->imagepath .",". $newz->breadtext .",". $newz->readmoretext;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(auth()->guest()){
            return redirect("/news");
        }
        $news = News::find($id);
        return view('news.edit',compact('news','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required',
            'subtitle' => 'required'
        ]);

if($request->get('imagepath') == null){

}
        $news = News::find($id);
        $news->title = $request->get('title');
        $news->subtitle = $request->get('subtitle');
        if($request->file('imagepath') == null){
            
        }
        else{
            // deleting old file
            $oldpath = $news->imagepath;
            Storage::disk('public_uploads')->delete($oldpath);

            $file = $request->file('imagepath');
            $filename =$request->get('imagepath');
            if(!$path = Storage::disk('public_uploads')->put($filename, $file)) {
                return false;
            }
            $news->imagepath = $path;

        }
        $news->breadtext = $request->get('breadtext');
        $news->readmoretext = $request->get('readmoretext');

        $news->save();
        //return redirect()->route('message.index')->with('success','Data updated');
        return redirect("/news")->with('success','Post updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $news = News::find($id);
        //return "found news=".$news->imagepath;
        $path = $news->imagepath;
        $file = Storage::disk('public_uploads')->get($path);

        if($file)
        {
            Storage::disk('public_uploads')->delete($path);
        }
        else
        {
            return redirect("/news")->with('error','file not found');

        }
        $news->delete();
        return redirect("/news")->with('success','Post deleted');
    }
}
