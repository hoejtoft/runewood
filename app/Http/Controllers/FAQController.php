<?php

namespace App\Http\Controllers;

use App\FAQ;
use Illuminate\Http\Request;

class FAQController extends Controller
{
    public function index()
    {
        $faq = FAQ::all()->toArray();
        return view('faq.index',compact('faq'));
    }
    public function addfaq()
    {
        //$order = Order::all()->toArray();
        return view('faq.addfaq');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'Question' => 'required',
            'Answer' => 'required'
        ]);
        $faq = new FAQ([
            'Question' => $request->get('Question'),
            'Answer' => $request->get('Answer')
        ]);
        try{
            $faq->save();
            \Session::flash('success', 'New faq created' );
            return redirect("/faq")->with('success','Faq created');
        }
        catch(\Exception $err){
            return "Error:" . $err->getMessage();
        }


    }
    public function edit($id)
    {
        if(auth()->guest()){
            return redirect("/faq");
        }
        $faq = FAQ::find($id);
        return view('faq.edit',compact('faq','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'Question' => 'required',
            'Answer' => 'required'
        ]);
        $faq = FAQ::find($id);
        $faq->Question = $request->get('Question');
        $faq->Answer = $request->get('Answer');
        $faq->save();
        //return redirect()->route('message.index')->with('success','Data updated');
        return redirect("/faq")->with('success','Data updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faq = FAQ::find($id);
        $faq->delete();
        return redirect("/faq")->with('success','Post deleted');
    }
}
