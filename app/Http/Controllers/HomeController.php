<?php

namespace App\Http\Controllers;

use App\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\submit;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gallery = DB::table('gallery')->where('galleryid', '=', 'runewood')->get()->toArray();
        $testimonial = DB::table('testimonial')->get()->toArray();
        return view('index',compact('gallery','testimonial'));
//        return view('index');
    }

    public function addeditgallery(){
        $gallery = DB::table('gallery')->where('galleryid', '=', 'runewood')->get()->toArray();
        //$gallery = Gallery::all()->toArray();
        return view('addeditgallery',compact('gallery'));
    }
    public function update(Request $request, $id)
    {

        $gallery = DB::table('gallery')->where('galleryid', '=', 'runewood')->get()->toArray();
        foreach($gallery as $entry => $galleryitem){
            $galleryrow = Gallery::find($galleryitem->id);

            if($request->file('imagepath'.$galleryitem->id) == null){

            }
            else{

                // deleting old file
                $oldpath = $galleryitem->imagepath;
                Storage::disk('public_uploads')->delete($oldpath);

                $file = $request->file('imagepath'.$galleryitem->id);
                $filename =$request->get('imagepath'.$galleryitem->id);
                if(!$path = Storage::disk('public_uploads')->put($filename, $file)) {
                    return false;
                }
                $galleryrow->imagepath = $path;
                $galleryrow->save();


            }
        }
        //return redirect()->route('message.index')->with('success','Data updated');
        return redirect("/editgallery");
    }

    public function delimg($id){

        $gallery = Gallery::find($id);

        $path = $gallery->imagepath;
        $file = $path != "" ? Storage::disk('public_uploads')->get($path) : "";
        if($file)
        {
            Storage::disk('public_uploads')->delete($path);
            $gallery->imagepath = "null";
            $gallery->save();
        }
        else{
            return redirect("/editgallery")->with('error','file not found');
        }
        return redirect("/editgallery");
    }

}
