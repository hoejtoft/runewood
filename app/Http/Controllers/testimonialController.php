<?php

namespace App\Http\Controllers;

use App\testimonial;
use Illuminate\Http\Request;

class testimonialController extends Controller
{
    public function index()
    {
        return view('testimonial.addtestimonial');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'testimonial_name' => 'required',
            'testimonial_text' => 'required'

        ]);
        try{
            $testimonial = new testimonial([
                'testimonial_name' => $request->get('testimonial_name'),
                'testimonial_profession' => $request->get('testimonial_profession'),
                'testimonial_rating' => $request->get('testimonial_rating'),
                'testimonial_text' => $request->get('testimonial_text'),
                'testimonial_image' => $request->get('testimonial_image'),
                'testimonial_productcategory' => $request->get('testimonial_productcategory')
            ]);
            $testimonial->save();
            //return redirect()->action('testimonialController@index');
            //return redirect("/Products/".$testimonial->testimonial_productcategory."s")->with('success','Post deleted');
            return redirect("/")->with('success','Post added');
        }
        catch(\Exception $err){
            return "Error:" . $err->getMessage();
        }
    }
    public function edit($id)
    {
        $testimonial = testimonial::find($id);
        return view('testimonial.edit',compact('testimonial','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'testimonial_name' => 'required',
            'testimonial_text' => 'required'

        ]);

        $testimonial = testimonial::find($id);
        $testimonial->testimonial_name = $request->get('testimonial_name');
        $testimonial->testimonial_profession = $request->get('testimonial_profession');
        $testimonial->testimonial_rating = $request->get('testimonial_rating');
        $testimonial->testimonial_text = $request->get('testimonial_text');
        $testimonial->testimonial_image = $request->get('testimonial_image');
        $testimonial->testimonial_productcategory = $request->get('testimonial_productcategory');
        $testimonial->save();
        //return redirect()->route('message.index')->with('success','Data updated');
        //return redirect("/Products/".$testimonial->testimonial_productcategory."s")->with('success','Data updated');
        return redirect("/")->with('success','Post updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testimonial = testimonial::find($id);
        $category = $testimonial->testimonial_productcategory;
        $testimonial->delete();
        //return redirect("/Products/".$category."s")->with('success','Post deleted');
        return redirect("/")->with('success','Post deleted');
        //return redirect()->route('message.index')->with('success','Post deleted');
    }
}
