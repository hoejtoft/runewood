<?php

namespace App\Http\Controllers;

use App\Mail\autoreply;
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\submit;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = Message::all()->toArray();
        return view('message.index',compact('messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {

            return view('message.createmsg');

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'first_name' => 'required',
            'email' => 'required',
            'country' => 'required',
            'text' => 'required'
        ]);
        $message = new Message([
        'first_name' => $request->get('first_name'),
        'email' => $request->get('email'),
        'country' => $request->get('country'),
        'text' => $request->get('text')
    ]);
try{
    $name = $message->first_name;
    $msg = $message->text;
    $recipient = $message->email;
    Mail::to('info@runewood.dk')->send(new submit($name,$msg));
    Mail::to($recipient)->send(new autoreply($name));

    $message->save();
    \Session::flash('success', 'Message sent' );
    return redirect()->route('message.create');
}
catch(\Exception $err){
    return $err->getMessage();
}


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(auth()->guest()){
            return redirect("/message");
        }
        $message = Message::find($id);
        return view('message.edit',compact('message','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'first_name' => 'required',
            'email' => 'required'
        ]);
        $message = Message::find($id);
        $message->first_name = $request->get('first_name');
        $message->email = $request->get('email');
        $message->save();
        //return redirect()->route('message.index')->with('success','Data updated');
        return redirect("/message")->with('success','Data updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $message =Message::find($id);
        $message->delete();
        return redirect("/message")->with('success','Post deleted');
        //return redirect()->route('message.index')->with('success','Post deleted');
    }


}
