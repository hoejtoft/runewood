<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    public function tempproducts()
    {
        //$order = Order::all()->toArray();
        return view('products.index_old');
    }
    public function listCabinets()
    {
        $product = DB::table('products')->where('category', '=', 'Cabinet')->get()->toArray();
        $testimonial = DB::table('testimonial')->where('testimonial_productcategory', '=', 'Cabinet')->get()->toArray();
        //$product = Product::all()->toArray();
        return view('products.cabinets',compact('product','testimonial'));
    }

    public function listWallhangers()
    {
        $product = DB::table('products')->where('category', '=', 'Wallhanger')->get()->toArray();
        $testimonial = DB::table('testimonial')->where('testimonial_productcategory', '=', 'Wallhanger')->get()->toArray();
        //$product = Product::all()->toArray();
        return view('products.wallhangers',compact('product','testimonial'));
    }
    public function showbasket()
    {
        //$order = Order::all()->toArray();
        return view('products.basket');
    }
    public function checkout()
    {
        //$order = Order::all()->toArray();
        $cart = Session::get('cart');
            return view('products.checkout',['products' => $cart->items, 'totalPrice' => $cart->totalPrice]);
    }
    public function addproduct()
    {
        //$order = Order::all()->toArray();
        return view('products.addproduct');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'productname' => 'required',
            'category' => 'required',
            'author' => 'required',
            'imagepath1' => 'required',
            'price' => 'required',
            'amount' => 'required',
            'description' => 'required',
        ]);


        try{
            if($request->hasfile('imagepath1')) {

                $path1 = "";
                $path2 = "";
                $path3 = "";
                $path4 = "";
                $file = $request->file('imagepath1');
                $filename = $request->get('imagepath1');
                if (!$path = Storage::disk('public_uploads')->put($filename, $file)) {
                    return false;
                }
                if($request->hasfile('imagepath2')){
                    $file1 = $request->file('imagepath2');
                    $filename1 = $request->get('imagepath2');
                    $path1 = Storage::disk('public_uploads')->put($filename1, $file1);
                }
                if($request->hasfile('imagepath3')){
                    $file2 = $request->file('imagepath3');
                    $filename2 = $request->get('imagepath3');
                    $path2 = Storage::disk('public_uploads')->put($filename2, $file2);
                }
                if($request->hasfile('imagepath4')){
                    $file3 = $request->file('imagepath4');
                    $filename3 = $request->get('imagepath4');
                    $path3 = Storage::disk('public_uploads')->put($filename3, $file3);
                }
                if($request->hasfile('imagepath5')){
                    $file4 = $request->file('imagepath5');
                    $filename4 = $request->get('imagepath5');
                    $path4 = Storage::disk('public_uploads')->put($filename4, $file4);
                }
                $product = new Product([
                    'productname' => $request->get('productname'),
                    'category' => $request->get('category'),
                    'author' => $request->get('author'),
                    'img1' => $path,
                    'img2' => $path1,
                    'img3' => $path2,
                    'img4' => $path3,
                    'img5' => $path4,
                    'price' => $request->get('price'),
                    'amount' => $request->get('amount'),
                    'description' => $request->get('description'),
                    'length' => $request->get('length'),
                    'heigth' => $request->get('heigth'),
                    'depth' => $request->get('depth'),
                    'capacity' => $request->get('capacity'),
                    'warranty' => $request->get('warranty'),
                    'materials' => $request->get('materials'),
                    'finish' => $request->get('finish'),
                    'features' => $request->get('features'),
                    'lighting' => $request->get('lighting'),
                ]);
                $product->save();
                \Session::flash('success', 'Product created' );
                return redirect("/Products/Add")->with('success','Product created');
            }
            else{
                return "false";
            }



        }
        catch(\Exception $err){
            return "Error:" . $err->getMessage();
        }


    }
    public function edit($id)
    {
        if(auth()->guest()){
            return redirect("/products/Wallhangers");
        }
        $product = Product::find($id);
        return view('products.edit',compact('product','id'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'productname' => 'required',
            'category' => 'required',
            'author' => 'required',
            'price' => 'required',
            'amount' => 'required',
            'description' => 'required',
        ]);

//        if($request->get('imagepath') == null){
//
//        }
        $product = Product::find($id);
        if($request->file('imagepath1') == null){

        }
        else{
            // deleting old file
            $oldpath = $product->img1;
            Storage::disk('public_uploads')->delete($oldpath);

            $file = $request->file('imagepath1');
            $filename =$request->get('imagepath1');
            if(!$path = Storage::disk('public_uploads')->put($filename, $file)) {
                return false;
            }
            $product->img1 = $path;

        }
        if($request->file('imagepath2') == null){

        }
        else{
            // deleting old file
            $oldpath = $product->img2;
            Storage::disk('public_uploads')->delete($oldpath);

            $file = $request->file('imagepath2');
            $filename =$request->get('imagepath2');
            if(!$path = Storage::disk('public_uploads')->put($filename, $file)) {
                return false;
            }
            $product->img2 = $path;

        }
        if($request->file('imagepath3') == null){

        }
        else{
            // deleting old file
            $oldpath = $product->img3;
            Storage::disk('public_uploads')->delete($oldpath);

            $file = $request->file('imagepath3');
            $filename =$request->get('imagepath3');
            if(!$path = Storage::disk('public_uploads')->put($filename, $file)) {
                return false;
            }
            $product->img3 = $path;

        }
        if($request->file('imagepath4') == null){

        }
        else{
            // deleting old file
            $oldpath = $product->img4;
            Storage::disk('public_uploads')->delete($oldpath);

            $file = $request->file('imagepath4');
            $filename =$request->get('imagepath4');
            if(!$path = Storage::disk('public_uploads')->put($filename, $file)) {
                return false;
            }
            $product->img4 = $path;

        }
        if($request->file('imagepath5') == null){

        }
        else{
            // deleting old file
            $oldpath = $product->img5;
            Storage::disk('public_uploads')->delete($oldpath);

            $file = $request->file('imagepath5');
            $filename =$request->get('imagepath5');
            if(!$path = Storage::disk('public_uploads')->put($filename, $file)) {
                return false;
            }
            $product->img5 = $path;

        }
        $product->productname = $request->get('productname');
        $product->category = $request->get('category');
        $product->author = $request->get('author');
        $product->price = $request->get('price');
        $product->amount = $request->get('amount');
        $product->description = $request->get('description');
        $product->capacity = $request->get('capacity');
        $product->warranty = $request->get('warranty');
        $product->length = $request->get('length');
        $product->heigth = $request->get('heigth');
        $product->depth = $request->get('depth');
        $product->materials = $request->get('materials');
        $product->finish = $request->get('finish');
        $product->features = $request->get('features');
        $product->lighting = $request->get('lighting');


        $product->save();
        //return redirect()->route('message.index')->with('success','Data updated');
        return redirect("/Products/".$product->category."s")->with('success','Product updated');
    }
    public function destroy($id)
    {

        $product = Product::find($id);
        //return "found news=".$news->imagepath;
        $path1 = $product->img1;
        $file1 = $path1 != "" ? Storage::disk('public_uploads')->get($path1) : "";
        $path2 = $product->img2;
        $file2 = $path2 != "" ? Storage::disk('public_uploads')->get($path2) : "";
        $path3 = $product->img3;
        $file3 = $path3 != "" ? Storage::disk('public_uploads')->get($path3) : "";
        $path4 = $product->img4;
        $file4 = $path4 != "" ? Storage::disk('public_uploads')->get($path4) : "";
        $path5 = $product->img5;
        $file5 = $path5 != "" ? Storage::disk('public_uploads')->get($path5) : "";
        if($file1)
        {
            Storage::disk('public_uploads')->delete($path1);
        }
        if($file2)
        {
            Storage::disk('public_uploads')->delete($path2);
        }
        if($file3)
        {
            Storage::disk('public_uploads')->delete($path3);
        }
        if($file4)
        {
            Storage::disk('public_uploads')->delete($path4);
        }
        if($file5)
        {
            Storage::disk('public_uploads')->delete($path5);
        }
        elseif(!$file1 && !$file2 && !$file3 && !$file4 && !$file5)
        {
            return redirect("/Products/".$product->category."s")->with('error','file not found');

        }
        $product->delete();
        return redirect("/Products/".$product->category."s")->with('success','Post deleted');
    }

    public function getAddToCart(Request $request, $id, $qty)
    {
        $product = Product::find($id);
        $cart = Session::has('cart') ? Session::get('cart') : null;
        if(!$cart)
        {
            $cart = new Cart($cart);
        }
        $productid = $product->id;
        $cart->add($product, $productid,$qty);

        Session::put('cart', $cart);
        //dd($request->session()->get('cart'));
        return redirect("/Products/".$product->category."s");
    }
    public function getCart(){
        if(!Session::has('cart')){
            return view('products.basket');
        }
        $cart = Session::get('cart');
        return view('products.basket',['products' => $cart->items, 'totalPrice' => $cart->totalPrice]);

    }
    public function deleteItem($id,$qty)
    {

        if(!Session::has('cart')){
            return view('products.basket');
        }
        $cart = Session::get('cart');
        //dd($qty);
        unset($cart->items[$id]);
        $cart->removeQty($qty);
        //put back in session array without deleted item
        Session::put('cart',$cart);

        if($cart->totalQty == 0){
            Session::forget('cart');
        }
        //then you can redirect or whatever you need
        //return view('products.basket',['products' => $cart->items]);
        return redirect("/Basket");
    }
    public function updateCartQty($id,$qty){
        if(!Session::has('cart')){
            return view('products.basket');
        }
        $cart = Session::get('cart');
        if($qty == 1){
            $cart->items[$id]['qty'] ++;
            $cart->addQty(1);
        }
        elseif($qty == 0){
            if($cart->items[$id]['qty'] == 0){

            }
            else{
                $cart->items[$id]['qty'] --;
                $cart->removeQty(1);
            }

        }
        else{
            $cart->items[$id]['qty'];
        }
        Session::put('cart',$cart);
        //return view('products.basket',['products' => $cart->items]);
        return redirect("/Basket");
    }
    public function resetBasket(){
        $cart = Session::get('cart');
        $cart = null;
        Session::put('cart',$cart);
        return view('products.basket');
    }
    public function clearbasket(){
        $gallery = DB::table('gallery')->where('galleryid', '=', 'runewood')->get()->toArray();
        $testimonial = DB::table('testimonial')->get()->toArray();

        $cart = Session::get('cart');
        $cart = null;
        Session::put('cart',$cart);
        return view('index',compact('gallery','testimonial'));
    }
}
