<?php

namespace App\Http\Controllers;
use App\Order;
use App\Mail\customermail;
use App\Mail\ordermail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$order = Order::all()->toArray();
        return view('products.index');
    }

    public function ShowData()
    {
        $order = Order::all()->toArray();
        return view('products.data',compact('order'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'Invoice_Name' => 'required',
            'Invoice_LastName' => 'required',
            'Invoice_Address' => 'required',
            'Invoice_Postal' => 'required',
            'Invoice_City' => 'required',
            'Invoice_Phone' => 'required',
            'Invoice_Email' => 'required'
        ]);
        $order = new Order([
            'first_name' => $request->get('Invoice_Name'),
            'last_name' => $request->get('Invoice_LastName'),
            'address' => $request->get('Invoice_Address'),
            'postal_code' => $request->get('Invoice_Postal'),
            'city' => $request->get('Invoice_City'),
            'phone' => $request->get('Invoice_Phone'),
            'email' => $request->get('Invoice_Email'),
            'country' => $request->get('country'),
            'productdetails' => $request->get('productdetails')
        ]);
        try{
            $name = $order->first_name;
            $lastname = $order->last_name;
            $address = $order->address;
            $postal_code = $order->postal_code;
            $city = $order->city;
            $phone = $order->phone;
            $email = $order->email;
            $country = $order->country;
            $recipient = $order->email;
            $productdetails = $order->productdetails;
            Mail::to('info@runewood.dk')->send(new ordermail($name,$lastname,$address,$postal_code,$city,$phone,$email,$country,$productdetails));
            Mail::to($recipient)->send(new customermail($name));

            $order->save();
            \Session::flash('success', 'Order placed' );
            return redirect("/checkout")->with('success','Order placed');
        }
        catch(\Exception $err){
            return "Error:" . $err->getMessage();
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(auth()->guest()){
            return redirect("/message");
        }
        $message = Message::find($id);
        return view('message.edit',compact('message','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'first_name' => 'required',
            'email' => 'required'
        ]);
        $message = Message::find($id);
        $message->first_name = $request->get('first_name');
        $message->email = $request->get('email');
        $message->save();
        //return redirect()->route('message.index')->with('success','Data updated');
        return redirect("/message")->with('success','Data updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order =Order::find($id);
        $order->delete();
        return redirect("/orderdata")->with('success','Post deleted');
        //return redirect()->route('message.index')->with('success','Post deleted');
    }
}
