<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->guest()){
            return redirect("/");
        }

        if(auth()->user()->userlevel != 1){
            return redirect("/");

        }
        else{
            return $next($request);

        }

//        if(auth()->user()->email == "jonathanhoejtoft@gmail.com" || auth()->user()->email == "jadams@runewood.dk" || auth()->user()->email == "noble@runewood.dk"){
//            return $next($request);
//        }

}
}
