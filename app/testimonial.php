<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class testimonial extends Model
{
    protected $table = 'testimonial';
    protected $fillable = [
        'testimonial_name',
        'testimonial_profession',
        'testimonial_rating',
        'testimonial_text',
        'testimonial_image',
        'testimonial_productcategory'
    ];
}
