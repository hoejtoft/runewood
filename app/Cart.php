<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 21-11-2018
 * Time: 15:23
 */

namespace App;


class Cart {
    public $items = null;
    public $itemQty = 0;
    public $totalQty = 0;
    public $totalPrice = 0;

    public function __contruct($oldcart){
        if($oldcart){
            $this->itemQty = $oldcart->itemQty;
            $this->items = $oldcart->items;
            $this->totalQty = $oldcart->totalQty;
            $this->totalPrice = $oldcart->totalPrice;
        }

    }

    public function add($item, $id,$qty){
        $storedItem = ['qty' => 0, 'price' => $item->price,'item' => $item];
        if($this->items){
            if(array_key_exists($id,$this->items)){
                $storedItem = $this->items[$id];
            }
        }
        $storedItem['qty'] += $qty;
        $storedItem['price'] = $item->price * $storedItem['qty'];
        $this->items[$id] = $storedItem;
        $this->totalQty += $qty;
        $this->totalPrice += $item->price;
    }
    public function removeQty($qty){
        $this->totalQty = $this->totalQty <=0 ? 0 : $this->totalQty  - $qty;
    }
    public function addQty($qty){
        $this->totalQty = $this->totalQty <=0 ? 0 : $this->totalQty  + $qty;
    }
}