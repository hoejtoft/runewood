<?php
use Illuminate\Support\Facades\Mail;
use App\Mail\submit;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('index');
//});

Route::get("/", "HomeController@index");

Route::get('/comingsoon', function () {
    return view('comingsoon');
});
Route::get('/about', function () {
    return view('about');
});
//Route::get('/news', function () {
//    return view('news');
//});

Route::resource('message','MessageController');
Route::resource('news','NewsController');
Route::resource('products','OrderController');
Route::resource('product','ProductController');
Route::resource('faq','FAQController');
Route::resource('home','HomeController');
Route::resource('testimonial','testimonialController');

Auth::routes();

Route::get('/addtocart/{id}/{qty}',[
    'uses' => 'ProductController@getAddToCart',
    'as' => 'product.addToCart'
]);
Route::get('/Basket/',[
    'uses' => 'ProductController@getCart',
    'as' => 'product.shoppingCart'
]);
Route::get('/Basket1/{id}/{qty}',[
    'uses' => 'ProductController@deleteItem',
    'as' => 'product.deleteProduct'
]);
Route::get('/Basket2/{id}/{qty}',[
    'uses' => 'ProductController@updateCartQty',
    'as' => 'product.updateProduct'
]);
Route::get('/Resetbasket',[
    'uses' => 'ProductController@resetBasket',
    'as' => 'product.resetCart'
]);
Route::get('/clearbasket',[
    'uses' => 'ProductController@clearbasket',
    'as' => 'product.clearbasket'
]);
Route::get('/Delimg/{id}',[
    'uses' => 'HomeController@delimg',
    'as' => 'home.deleteslide'
]);
//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/editgallery', 'HomeController@addeditgallery');

Route::get("message", "MessageController@index")->middleware("admin");

Route::get("/news", "NewsController@index");
Route::get("/checkout", "ProductController@checkout");

Route::get("/products", "ProductController@tempproducts");

Route::get("/Products/Cabinets", "ProductController@listCabinets");

Route::get("/Products/Wallhangers", "ProductController@listWallhangers");

//Route::get("/Basket", "ProductController@showbasket");

Route::get("/Products/Add", "ProductController@addproduct");

Route::get("/orderdata", "OrderController@ShowData")->middleware("admin");

Route::get("addpost", "NewsController@create")->middleware("admin");

Route::get('/send/email', 'MailController@mail');

Route::get("/faq", "FAQController@index");
Route::get("/addfaq", "FAQController@addfaq");
Route::get("/addtestimonial", "testimonialController@index");