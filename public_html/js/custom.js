$( document ).ready(function() {
    SetCountryFromIP("country");
    toggletest();
    ToggleScrollTop();
    ScrollToTop();
    $(":file").filestyle();
    CharacterCount();
    showImage();
    ValidateDelete();
    //calculateBasket();
    showPreImage();
    CalculateCartOnLoad();
    correctAmountToCart();
    updateQty();
    numberformat();
    commasOnDigits();
    $('#wysiwyg').summernote({
        height: 200
    });
    floatPlaceholder();
    initFloatOnLoad();
    initOwlCarousel();
    redirectToHome("checkout-success");

    $('#myCarousel').carousel({
        interval: 4000
    });
    $('.first_slide_1').addClass("active");
    $("#slideitem_1").addClass("active");
    $("#slideindicator_0").addClass("active");


// handles the carousel thumbnails
    $('[id^=carousel-selector-]').click( function(){
        var id_selector = $(this).attr("id");
        var id = id_selector.substr(id_selector.length -1);
        id = parseInt(id);
        $('#myCarousel').carousel(id);
        $('[id^=carousel-selector-]').removeClass('selected');
        $(this).addClass('selected');

    });

// when the carousel slides, auto update
    $('#myCarousel').on('slid', function (e) {
        var id = $('.item.active').data('slide-number');
        id = parseInt(id);
        $('[id^=carousel-selector-]').removeClass('selected');
        $('[id=carousel-selector-'+id+']').addClass('selected');
    });

});

function CalculateCartOnLoad(){
    $(".cQuantityInput").each(function(){
        var subtotal = 0;
        var amount = $(this).val();
        var thisprice = $(this).parent().next().children().html();
        var thistotal = $(this).parent().next().next().children();
        total = (amount*thisprice).toFixed(2);
        $(thistotal).html(total);
        $('[id^=totalproductprice_]').each(function(){
            subtotal +=  parseFloat($(this).html());

        });
        var shipping = $("#Shipping_price").html();
        $("#Subtotal_price").html(numberWithCommas(subtotal.toFixed(2)));
        var endprice = (subtotal + parseFloat(shipping)).toFixed(2);
        endprice = numberWithCommas(endprice);
        $("#total_price").html(endprice);

    });
    addCommaOnnumbers();
}
function addCommaOnnumbers(){
    $('[id^=productprice_]').each(function(){
        var CorrectedAmount = numberWithCommas($(this).html());
        $(this).html(CorrectedAmount);

    });
    $('[id^=totalproductprice_]').each(function(){
        var CorrectedAmount = numberWithCommas($(this).html());
        $(this).html(CorrectedAmount);

    });
}
function commasOnDigits(){

    $('[id^=products_price]').each(function(){
        var CorrectedAmount = numberWithCommas($(this).html());
        $(this).html(CorrectedAmount);

    });
}

function updateQty(){
    $(".numsubtract").click(function(){
        alert("dsf");
        var numinputval = $(this).next();
        var decrement = numinputval.val() <=0 ? 0 : numinputval.val() -1;
        $(numinputval).val(decrement);
        //CalculateCartOnLoad();
    });
    $(".numadd").click(function(){
        var numinputval = $(this).prev();
        var increment = numinputval.val() <0 ? 0 : parseInt(numinputval.val())  +1;
        $(numinputval).val(increment);
        //CalculateCartOnLoad();
    });
}

function calculateBasket(){
    var price = 0;
    var total = 0;
    $(".cQuantityInput").change(function(){
        var subtotal = 0;
        var amount = $(this).val();
        var thisprice = $(this).parent().next().children().html();
        var thistotal = $(this).parent().next().next().children();
        total = (amount*thisprice);
        $(thistotal).html(total);
        $('[id^=totalproductprice_]').each(function(){
           subtotal +=  parseFloat($(this).html());
        });
        var shipping = $("#Shipping_price").html();
        $("#Subtotal_price").html(subtotal.toFixed(2));
        var endprice = subtotal + parseFloat(shipping).toFixed(2);
        endprice = numberWithCommas(endprice);
        console.log(endprice);
        $("#total_price").html(endprice);

    });
}

function ipLookUp () {
    var country = "t";
    $.ajax('http://ip-api.com/json')
        .then(function success(response) {
            console.log('User\'s Location Data is ', response);
            console.log('User\'s Country', response.country);
            country = response.country;
            return country;
        },


        function fail(data, status) {
            console.log('Request failed.  Returned status of',
                status);
            country = "ert";
        }
    );
console.log(country);
    return country;
}

function SetCountryFromIP(inputfield){
    $.ajax({
        type: "POST",
        url: "http://ip-api.com/json",
        success: function(response){
            var country_input = $('[name='+inputfield+']');
            country_input.val(response.country);
        }
    });
}
function setToggleReadMore(){
    $('.nav-toggle').click(function () {
        var collapse_content_selector = $(this).attr('href');
        var toggle_switch = $(this);
        $(collapse_content_selector).toggle(function () {
            if ($(this).css('display') == 'none') {
                toggle_switch.html('Read More');
            } else {
                toggle_switch.html('Read Less');
            }
        });
    });
}
function toggletest(){
    $(".nav-toggle").click(function() {
        var speed = 300;
        var RM_selector = "#"+$(this).attr('id');
        var text_selector = "#"+$(this).prev().attr('id');
        var elem = $(RM_selector).text();
        if (elem == "Read More") {
            //Stuff to do when btn is in the read more state
            $(RM_selector).text("Read Less");
            $(text_selector).fadeIn(speed);
        } else {
            //Stuff to do when btn is in the read less state
            $(RM_selector).text("Read More");
            $(text_selector).fadeOut(150);
        }
    });
}
function ToggleScrollTop(){
    $(window).scroll(function() {
        var height = $(window).scrollTop();

        if(height  > 100) {
            $("#to-top").fadeIn(100);
        }
        else{
            $("#to-top").fadeOut(100);
        }
    });
}
function ScrollToTop(){
    $("#to-top").click(function(){
        $('html').animate({scrollTop:0}, 'slow');//IE, FF
        $('body').animate({scrollTop:0}, 'slow');//chrome, don't know if Safari works
    });
}
function CharacterCount(){
    var readmore_elem = $("textarea[name='readmoretext']");
    readmore_elem.css("display","none");
    $("#editreadmore").css("display","block");
    $('#newstext').keyup(function(){
        var amount = $('#newstext').val();
        var maxlength = 300;
        var charleft = maxlength-amount.length;
        $('#characterCount').html(charleft);
        if(charleft == 0){
            $('#newstext').addClass('errorBlur');
            readmore_elem.fadeIn(100);
        }
        else{
            $('#newstext').removeClass('errorBlur');
            readmore_elem.fadeOut(100);
        }
    })
}
function showImage(){
    $('#imagepath').change( function(event) {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        $("#tempimg").fadeIn("fast").attr('src',URL.createObjectURL(event.target.files[0]));

        $("#disp_tmp_path").html("Temporary Path(Copy it and try pasting it in browser address bar) --> <strong>["+tmppath+"]</strong>");
    });
}
function showPreImage(){
    $('[id^=imagepath]').change( function(event) {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        var placeholder = $(this).next().next().attr("id");
        var fadeimg = $(this).next().next().next().attr("id");
        $("#"+fadeimg).css("opacity","0.3");
        $("#"+placeholder+"").fadeIn("fast").attr('src',URL.createObjectURL(event.target.files[0]));

        $("#disp_tmp_path").html("Temporary Path(Copy it and try pasting it in browser address bar) --> <strong>["+tmppath+"]</strong>");
    });
}

function ValidateDelete(){
    $('.delete_form').on('submit',function(){
        if(confirm("Do you want to delete the post?")){
            return true;
        }
        else{
            return false;
        }
    });
}
function correctAmountToCart(){
    $(".amountSelected").change(function(){
        var qty = $(this).val();
        var linkAttr = $(this).parent().next().children().children().attr("href");
        var rest = linkAttr.substring(0, linkAttr.lastIndexOf("/") + 1);
        var last = linkAttr.substring(linkAttr.lastIndexOf("/") + 1, linkAttr.length);
        $(this).parent().next().children().children().attr("href",rest+""+qty);
    });
}
function numberformat(){

    $("#numberjs").keyup(function(){
        var number = $("#numberjs").val();
        var digit_partone = 0;
        var digit_parttwo = 0;
        var finaldigit = 0;
        if(number.length>=4){
            //console.log("lenght:" + (number.length))
            digit_partone = number.substring((number.length-4),(number.length-4)+(number.length-3));
            //console.log("digit_partone: " + digit_partone);
            digit_parttwo = number.substring(digit_partone.length);
            //console.log("digit_parttwo: "+ digit_parttwo)
            finaldigit = digit_partone + "," + digit_parttwo;
            console.log(finaldigit);
            $("#numberjs").val(finaldigit);
        }
        console.log(parseFloat(number).toFixed(2));

    });
}
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function floatPlaceholder(){
    $(function(){
        $('.floatplacehold').each(function(e){
            var placeholder = $(this).attr('placeholder');
            console.log(placeholder);
            $(this).wrap('<div class="inputwrapper"></div>').before('<span class="placeholdit">'+placeholder+'</span>');
            $(this).on('focus',function(){
                var inputContent = $(this).val();
                if(inputContent == ''){
                    $(this).prev().addClass('visible');
                }

            });
            $(this).on('blur',function(){
                var inputContent = $(this).val();
                if(inputContent == ''){
                    $(this).prev().removeClass('visible');
                }
            });
        });
    });
}
function initFloatOnLoad(){
    setTimeout(function(){
        $('.placeholdit').each(function(){
            var value = $(this).next().val();
            if(value == ""){
                console.log("empty");
            }
            else{
                $(this).addClass('visible');
            }

        });
    }, 1000);

}
function initOwlCarousel(){
    // Reviews slider
    $(".reviews .owl-carousel").owlCarousel({
        loop: true,
        margin: 35,
        nav: true,
        center: true,
        autoplay: true,
        autoplayTimeout:8000,
        navText: [
            '<i class="fa fa-angle-left"></i>',
            '<i class="fa fa-angle-right"></i>'
        ],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            750: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    });
}
function redirectToHome(id){
    $("#"+id).click(function(){
        window.location.href = "/clearbasket";
    });


}
